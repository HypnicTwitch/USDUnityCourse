

		    *********** MOVES Institute **********

		***************   12/12/2005   ***************

"CVN68" was created by William Clapham at Delta3d and may be freely used and distributed.   

"CVN68" was created in 3dsmax7 and exported using 0sgexp 0.93 prerelease 1

	- Texture(s): 2
	     	- grey
	     	- island
	   	- islandTowerArm
	     	- radarArray
	     	- bumpers
	     	- hull_tile
		- flightDeck
		- sides
		- back


	- units:
  	     - Meters 

	- dimensions:
  	   	- x = 79.8 meters
  	    	- y = 331.95 meters
		- z = 71 meters

	- Orientation:
	     -Z -> UP
	     -Y -> Forward

	- DOF(s):
	     - N/A

	- Switch(es):
	     - N/A

	- LOD poly count and switch distances (switch 'IN')
	     - Low -> 2,871 polys -> no LOD switch


	- Animation(s)

        - DIS
             - kind="1" domain="3" category="1" country="225" subcategory="1" specific="1"

