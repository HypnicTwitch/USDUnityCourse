		***********MOVES Institute**********
		************ 20JUNE2005 ************

This model is being distributed by the Delta3D team.

LSD-49 (USS Harpers Ferry) Dock Landing Ship

Models and textures contained in this directory were donated to the MOVES Institute by Dynamic Animation Systems, Inc. (DAS). 
They are free for use and distribution. Any distribution must be accompanied by this readme.txt with full credit given to it's creators.

Dynamic Animation Systems can be found on the web at: http://www.d-a-s.com
This model was originally downloaded from http://www.delta3d.org

	- units:
  	     Meters

	- Orientation:
	     Z -> UP
	     Y -> Forward

	- DOFs:
	     none

	- Switches:
	     none

	- LODs:
		4

