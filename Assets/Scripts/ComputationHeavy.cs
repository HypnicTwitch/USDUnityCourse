﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputationHeavy : MonoBehaviour {

    public int numVectors = 500;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(RandomMath());
	}

    float RandomMath()
    {
        //return 4.0f;

        List<Vector3> vectors = new List<Vector3>();

        // Initialize Vectors to random range
        InitializeVectors(ref vectors);

        // Caluclate Distances between vectors
        List<float> distances = CalculateDistances(vectors);
        
        // Calculate Angles between vectors
        List<float> angles = CalculateAngles(vectors);

        // Do some Trig math on all of the angles/distances
        List<float> calculated = new List<float>();        
        for(int i = 0; i < distances.Count; i++)
        {
            calculated.Add(TrigMathFunction(distances[i], angles[i]));
        }

        //float useless = CompressValues(calculated);

        return angles[0];

    }

    void InitializeVectors(ref List<Vector3> vecList)
    {
        for (int i = 0; i < numVectors; i++)
        {
            vecList.Add(GetRandomVector(float.MinValue, float.MaxValue));
        }
    }

    Vector3 GetRandomVector(float minRange, float maxRange)
    {
        return new Vector3(
            Random.Range(minRange, maxRange),
            Random.Range(minRange, maxRange),
            Random.Range(minRange, maxRange));
    }

    List<float> CalculateDistances(List<Vector3> vectors)
    {
        List<float> distances = new List<float>();

        foreach(Vector3 firstVector in vectors)
        {
            foreach (Vector3 secondVector in vectors)
            {
                //distances.Add((firstVector - secondVector).magnitude);
                distances.Add(Vector3.Distance(firstVector, secondVector));
            }
        }

        return distances;
    }

    List<float> CalculateAngles(List<Vector3> vectors)
    {
        List<float> angles = new List<float>();

        foreach (Vector3 firstVector in vectors)
        {
            foreach (Vector3 secondVector in vectors)
            {
                angles.Add(Vector3.Angle(firstVector, secondVector));
            }
        }

        return angles;
    }


    float TrigMathFunction(float distance, float angle)
    {
        return distance * Mathf.Sin(angle * Mathf.Deg2Rad);
    }


    float CompressValues(List<float> inputValues)
    {
        float finalValue = inputValues[0];

        float adjustedValue;

        for(int i = 1; i < inputValues.Count; i++)
        {
            adjustedValue = inputValues[i];

            // If finalValue less than one
            if (finalValue < 0)
            {
                // Insure adjusted is positive
                adjustedValue *= Mathf.Sign(inputValues[i]);
                //adjustedValue = Mathf.Abs(inputValues[i]);
            }
            // If finalValue greater than one
            else
            {
                // Insure adjusted is negative
                adjustedValue *= -Mathf.Sign(inputValues[i]);
                //adjustedValue = Mathf.Abs(inputValues[i]);
            }

            finalValue += adjustedValue;
        }

        return finalValue;
    }
}
