﻿using System;

namespace AssemblyCSharp
{
	public interface IPlayerInput
	{
		Boolean handleLeftClick();
		Boolean handleMiddleClick();
		Boolean handleRightClick();

		Boolean handleForwardKey();
		Boolean handleBackwardKey();
	}
}

