﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script should be attached to an object, will move the object from it's start position, to the left, until it reaches its end position.
/// </summary>
public class BenSatMotion : MonoBehaviour {

	public Vector3 satStartPosition;
	public Vector3 satEndPosition;
	public float satSpeed = 1.0f;
	public Camera freefallCamera;
	public float freefallSpeed = 90.0f;
	public int freefallTime = 20;
	public float freefallRotationSpeed = 1.0f;

	void Start () {
		PositionSatellite (satStartPosition);
		StartCoroutine (MoveSatellite ());
	}

	void Update() {
		//this.transform.position = Vector3.Lerp (satStartPosition, satEndPosition, 2000.0f);
	}
	
	/// <summary>
	/// Moves the satellite.
	/// </summary>
	/// <returns>The satellite.</returns>
	IEnumerator MoveSatellite() {
		StartCoroutine (DropCamera ());
		while (this.transform.position.x >= satEndPosition.x) {
			//minus == move left
			this.transform.position += new Vector3(-satSpeed,0)*Time.deltaTime;
			yield return null;
		}
	}

	/// <summary>
	/// Positions the satellite.
	/// </summary>
	/// <param name="startPos">Start position.</param>
	private void PositionSatellite(Vector3 startPos){
		Debug.Log ("X: " + startPos.x + ", Y: " + startPos.y + ", Z: " + startPos.z);
		this.transform.position = startPos;
	}

	/// <summary>
	/// Drops the camera.
	/// </summary>
	private IEnumerator DropCamera() {
		yield return new WaitForSeconds(5);
		while (freefallCamera.transform.position.y >= 25.3f) {
			freefallCamera.transform.position += new Vector3 (0, -freefallSpeed, 0) * Time.deltaTime;
			if (freefallCamera.transform.rotation.x >= 0) {
				freefallCamera.transform.Rotate ( -90.0f * Time.deltaTime,0,0);
			}
			yield return null;
		}
	}
}
