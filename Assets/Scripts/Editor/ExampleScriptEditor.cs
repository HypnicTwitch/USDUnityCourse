﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ExampleScript))]
public class ExampleScriptEditor : Editor
{
    // Instance of the script this is editing
    ExampleScript exampleScript;

    // Drawer toggles
    bool usingLocal;
    bool usingWorld;

    private void OnEnable()
    {
        // Associate this editor script instance with the actual script instance
        exampleScript = (ExampleScript)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector(); // Draw the script like normal
        return;

        EditorGUILayout.PropertyField(serializedObject.FindProperty("objectToReference"));

        // -= Local Toggle=- 
        usingLocal = GUILayout.Toggle(usingLocal, "Use Local Values");
        if(usingLocal)
        {
            GUILayout.BeginVertical("Box"); // Put localValues in a box
            // Serialize the localValues property
            SerializedProperty localValuesProperty = serializedObject.FindProperty("localValues");
            EditorGUILayout.PropertyField(localValuesProperty, true);   // Display localValues
            GUILayout.EndVertical();    // End localValues Box
        }

        // -= World Toggle=- 
        usingWorld = GUILayout.Toggle(usingWorld, "Use World Values");
        if (usingWorld)
        {
            GUILayout.BeginVertical("Box"); // Put worldValues in a box
            // Serialize the worldValues property
            SerializedProperty worldValuesProperty = serializedObject.FindProperty("worldValues");
            EditorGUILayout.PropertyField(worldValuesProperty, true);   // display worldValues

            // Call WorldOnlyFunction button
            if (GUILayout.Button("World Function"))
            {
                exampleScript.WorldOnlyFunction();
            }

            EditorGUILayout.Space();
            GUILayout.EndVertical();    // End worldValues box
        }

        // Call FunctionToTest Button
        if (GUILayout.Button("Test Button"))
        {
            exampleScript.FunctionToTest();
        }

        // Ensure changed properties are actually applied to our target script
        serializedObject.ApplyModifiedProperties();
    }
}