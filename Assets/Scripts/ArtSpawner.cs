﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtSpawner : MonoBehaviour {

    public GameObject prefabToSpawn;
    public int numberToSpawn = 100;
    float spawnDistance = 100.0f;

	// Use this for initialization
	void Start () {
        SpawnObjects();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnObjects()
    {
        for(int i = 0; i < numberToSpawn; i++)
        {
            Instantiate(prefabToSpawn, GetRandomVector(-spawnDistance, spawnDistance), Quaternion.identity);
        }
    }

    Vector3 GetRandomVector(float minRange, float maxRange)
    {
        return new Vector3(
            Random.Range(minRange, maxRange),
            Random.Range(minRange, maxRange),
            Random.Range(minRange, maxRange));
    }
}
