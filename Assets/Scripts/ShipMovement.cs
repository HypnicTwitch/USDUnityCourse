﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    public float speed = 5.0f;
    public float turnSpeed = 30.0f;
    public GameObject destinationPoint = null;

    private bool isMoving = false;
    private Vector3 destination;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        HandleKeyboardPresses();

        HandleMouseClick();

        if (isMoving)
        {
            Translate(destination);
        }
    }

    /// <summary>
    /// Handle keyboard presses
    /// </summary>
    private void HandleKeyboardPresses()
    {
        // Forward
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            Translate(forward: true);
        }

        // Back
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            Translate(forward: false);
        }

        // Turn Left
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            Rotate(left: true);
        }

        // Turn Right
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            Rotate(left: false);
        }
    }

    /// <summary>
    /// Handle mouse clicks
    /// </summary>
    private void HandleMouseClick()
    {
        // Left mouse click
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                StopMoving();

                // Position of the hit can be accessed by hit.point
                destination = new Vector3(hit.point.x, 0, hit.point.z);
                Instantiate(destinationPoint, destination, Quaternion.identity);

                transform.LookAt(destination);

                isMoving = true;
            }
        }
    }

    /// <summary>
    /// Move the ship forward or backward
    /// </summary>
    /// <param name="forward"></param>
    private void Translate(bool forward)
    {
        StopMoving();
        float step = speed * Time.deltaTime;

        if (forward)
        {
            transform.Translate(Vector3.forward * step);
        }
        else
        {
            transform.Translate(Vector3.back * step);
        }
    }

    /// <summary>
    /// Move ship to a position
    /// </summary>
    /// <param name="position"></param>
    private void Translate(Vector3 position)
    {
        transform.position = Vector3.MoveTowards(transform.position, position, speed * Time.deltaTime);
    }

    /// <summary>
    /// Turn the ship left or right
    /// </summary>
    /// <param name="left"></param>
    private void Rotate(bool left)
    {
        StopMoving();

        Vector3 rotateAmount = Vector3.up;
        rotateAmount *= turnSpeed * Time.deltaTime;

        if (left)
        {
            rotateAmount *= -1.0f;
        }

        transform.Rotate(rotateAmount);
    }

    /// <summary>
    /// Stop the ship from moving
    /// </summary>
    public void StopMoving()
    {
        isMoving = false;

        // Delete all other destination points
        GameObject[] destinationPoints = GameObject.FindGameObjectsWithTag("DestinationPoint");
        for (int i = 0; i < destinationPoints.Length; i++)
        {
            Destroy(destinationPoints[i]);
        }
        destinationPoints = null;
    }
}
