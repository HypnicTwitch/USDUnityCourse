﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PointClickDestination : MonoBehaviour {

    public float speed = 5.0f;
    public GameObject destinationPoint = null;
    private bool isMoving = false;

    private Vector3 destination;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            HandleMouseClick();
        }

        if (isMoving)
        {
            Translate(destination);
        }
	}

    public void StopMoving()
    {
        isMoving = false;
    }

    private void HandleMouseClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000.0f))
        {
            // Position of the hit can be accessed by hit.point
            destination = hit.point;
            Instantiate(destinationPoint, destination, Quaternion.identity);
            transform.LookAt(destination);
            isMoving = true;
        }
    }

    private void Translate(Vector3 newPosition)
    {
        transform.position = Vector3.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);
    }
}
