﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteCam : MonoBehaviour {

	public Camera satellite;
	public Screenshot myshot;
	private int count=0;
	// Use this for initialization
	void Start () {
		satellite = this.gameObject.GetComponent<Camera> ();
		satellite.transform.position += new Vector3 (10, 0, 0);
		myshot = this.gameObject.GetComponent<Screenshot> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (count < 3) {
			satellite.transform.position += new Vector3 (10, 0, 0);
			StartCoroutine (myshot.WaitAndTakeScreenshot ());
			count++;
		}
	}

}
