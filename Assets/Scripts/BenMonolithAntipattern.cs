﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BenMonolithAntipattern : MonoBehaviour {

	private Vector3 clickedPoint;
	public GameObject destinationPrefab;
	public GameObject mineSpawner;
	public float movementSpeed = 100.0f;
	public bool shipInMotion = false;


	// Use this for initialization
	void Start () {
		destinationPrefab = Resources.Load<GameObject> ("DestinationPoint");
		mineSpawner = Instantiate(new GameObject());
		//mineSpawner.AddComponent ("");
		//Instantiate(Resources.Load<GameObject>("Mine"), new Vector3(0,0,0),Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		//accept the user's mouse input
		if (Input.GetMouseButtonDown (0)) {
			handleLeftClick ();
		} else if (Input.GetMouseButtonDown (2)) {
			handleMiddleClick ();
		} else if (Input.GetMouseButtonDown (1)) {
			handleRightClick ();
		}

		//apply the user's mouse input to the game
		ApplyShipMovement (clickedPoint);

	}

	private void handleLeftClick() {
		ClearNavPoints ();
		RaycastHit hit;
		int oceanLayer = 1 << LayerMask.NameToLayer ("Ocean");
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if( Physics.Raycast(ray, out hit, 1000.0f, oceanLayer) ) {
			clickedPoint = new Vector3 (hit.point.x, 0, hit.point.z);
			SpawnDestinationPoint (clickedPoint);
			StartMoving ();
			changeShipDirection (clickedPoint);
		}
	}

	private void handleMiddleClick() {
		Debug.Log ("Middle Click");
	}

	private void handleRightClick() {
		Debug.Log ("Right Click");
	}

	public void ApplyShipMovement(Vector3 userInput) {
		if(isShipMoving()) {
			transform.position = Vector3.MoveTowards (transform.position, userInput, movementSpeed * Time.deltaTime);
		}
	}

	public void ClearNavPoints() {
		GameObject[] destinationPoints = GameObject.FindGameObjectsWithTag ("DestinationPoint");
		Debug.Log ("Nav points to be cleared: " + destinationPoints.Length);
		for (int i = 0; i < destinationPoints.Length; i++) {
			Destroy (destinationPoints [i]);
		}
	}

	public void StartMoving() {
		shipInMotion = true;
		Debug.Log ("StartMoving called");
	}

	public void StopMoving() {
		shipInMotion = false;
		Debug.Log ("StopMoving called");
		ClearNavPoints ();
	}

	public bool isShipMoving() {
		return shipInMotion;
	}

	/// <summary>
	/// Spawns the destination point in the form of a prefab
	/// </summary>
	public void SpawnDestinationPoint(Vector3 location) {
		Instantiate (destinationPrefab,location,Quaternion.identity);
	}

	public void changeShipDirection(Vector3 pointToHere) {
		transform.LookAt (pointToHere);
	}

	private void readMineFile() {
		//StreamReader textFileReader = new StreamReader ("../Resources/Mines.txt");
		//Debug.Log(textFileReader);
	}

}
