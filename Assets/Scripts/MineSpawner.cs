﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MineSpawner : MonoBehaviour {

    public GameObject minePrefab = null;

    private List<Vector3> minePositions = new List<Vector3>();

    // Use this for initialization
    void Start () {
        GameObject mineParent = new GameObject();

        // Read mine positions
        ReadFile();

        // Spawn mines
        foreach (Vector3 pos in minePositions)
        {
            Instantiate(minePrefab, pos, Quaternion.identity, mineParent.transform);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void ReadFile()
    {
        ReadTextFile("/Resources/Mines.txt");
    }

    private void ReadTextFile(string fileName)
    {
        string actualPath = Application.dataPath + fileName;

        using (StreamReader inputStream = new StreamReader(actualPath))
        {
            while (!inputStream.EndOfStream)
            {
                string line = inputStream.ReadLine();

                string[] values = line.Split(',');

                minePositions.Add(new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2])));
            }
        }
    }
}
