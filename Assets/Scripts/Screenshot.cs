﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screenshot : MonoBehaviour {
    public RenderTexture satelliteRenderTexture;

    void Start () {
        StartCoroutine(WaitAndTakeScreenshot());
	}

    public IEnumerator WaitAndTakeScreenshot()
    {
        yield return new WaitForSeconds(3.0f);

        StartCoroutine(SaveImage());
    }

	public IEnumerator SaveImage()
    {
        //Small delay to make sure everything is in the correct position
        //yield return new WaitForSeconds(.01f);
		yield return new WaitForEndOfFrame ();

        //Setup a place in memory to store the image
        Texture2D snap = new Texture2D(satelliteRenderTexture.width, satelliteRenderTexture.height);

        //Set the satellite render texture to be the 'active' render texture. This basically tells your GPU
        //which place to grab the image from when doing a texture copy.
        RenderTexture.active = satelliteRenderTexture;

        //This reads the pixels of the active render texture and saves them to memory.
        snap.ReadPixels(new Rect(0, 0, satelliteRenderTexture.width, satelliteRenderTexture.height), 0, 0, false);
        snap.Apply();

        //Reset the active render texture
        RenderTexture.active = null;

        //Save the image to disk.
        System.IO.File.WriteAllBytes(Application.dataPath + "/Images/" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss_fff") + ".png", snap.EncodeToPNG());

        Debug.Log("Screenshot Captured!");

        yield return null;
    }
}
