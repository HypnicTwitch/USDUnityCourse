﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
		Debug.Log ("collision detected.");
        if (other.CompareTag("Ship"))
        {
			other.GetComponent<ShipMovement>().StopMoving();
            Destroy(gameObject);
        }
    }
}
