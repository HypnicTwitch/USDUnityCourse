﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript : MonoBehaviour {

    public GameObject objectToReference;

    [Header("Local Settings")]
    public bool useLocalValues;
    public Vector3 localPositionOffset;
    public Vector3 localRotationOffset;
    public float someLocalValue;
    public string pathOfLocalFile;

    [Header("World Settings")]
    public bool useWorldValues;
    public Transform relavtiveTo;
    public Vector3 worldPositionOffset;
    public Vector3 worldRotationOffset;
    public float someWorldValue;

    //public WorldValues worldValues = new WorldValues();
    //public LocalValues localValues = new LocalValues();

    public void FunctionToTest()
    {
        Debug.Log("Editor Button Pressed");
    }

    public void WorldOnlyFunction()
    {
        Debug.Log("World Only Function Called");
    }
}

[System.Serializable]
public class LocalValues    // Note: Doesn't need to be a Monobehaviour
{
    public Vector3 localPositionOffset;
    public Vector3 localRotationOffset;
    public float someLocalValue;
    public string pathOfLocalFile;

    private Vector3 hiddenVector;   // Not visible in inspector because private
}

[System.Serializable]
public class WorldValues
{
    public Transform relavtiveTo;
    public Vector3 worldPositionOffset;
    public Vector3 worldRotationOffset;
    public float someWorldValue;

    [SerializeField]
    private float visibleFloat; // Private but visible in inspector

    [System.NonSerialized]
    public int hiddenInt;   // Public but not visible in inspector
}
