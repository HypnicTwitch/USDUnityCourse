# University of San Diego Unity Course
### taught by BEMR SSCPac

### Miscellaneous Notes
* Select an element that is off-screen, press 'F' to focus on it
** Also useful to realigning the camera and resetting the zoom scale when moving between UI and Scene elements:q

### Execution Order:
* https://docs.unity3d.com/Manual/ExecutionOrder.html
* useful graphic for visualizing all of the processes that occur for each rendered frame of a Unity game

### User Interface
* Hold down Alt, while selecting a pivot point for a UI Element, to move the pivot AND the element to the selected location.

### Render Texture
* Keep number of cameras in a scene low
* 
